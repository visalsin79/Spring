package com.visal.spring.min.project.demo.repositories;

import com.visal.spring.min.project.demo.models.Category;
import com.visal.spring.min.project.demo.repositories.providers.BookProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @Select("select * from tb_category order by id")
    List<Category> getAll();


    @Select("select count(*) from tb_category")
    Integer count();


    @Select("select * from tb_category where id=#{id}")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name")
    })
    Category findOne(@Param("id") Integer id);

    @InsertProvider(type = BookProvider.class, method = "create_category")
    boolean create_category(Category category);


    @Update("update tb_category set name=#{name} where id=#{id}")
    boolean update_category(Category category);

    @Delete("DELETE FROM tb_category where id=#{id}")
    boolean remove_category(Integer id);


}
