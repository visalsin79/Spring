package com.visal.spring.min.project.demo.services.impl;


import com.visal.spring.min.project.demo.models.Category;
import com.visal.spring.min.project.demo.repositories.CategoryRepository;
import com.visal.spring.min.project.demo.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAll() {
        return this.categoryRepository.getAll();
    }

    @Override
    public Category findOne(Integer id) {
        return this.categoryRepository.findOne(id);
    }

    @Override
    public Integer count() {
        return this.categoryRepository.count();
    }

    @Override
    public boolean create_category(Category category) {
        return this.categoryRepository.create_category(category);
    }

    @Override
    public boolean update_category(Category category) {
        return this.categoryRepository.update_category(category);
    }

    @Override
    public boolean remove_category(Integer id) {
        return this.categoryRepository.remove_category(id);
    }


}
